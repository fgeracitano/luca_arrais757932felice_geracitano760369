import static org.junit.Assert.*;

import org.junit.Test;


public class StringCalculatorTest {


	StringCalculator stringCalculator;
	
	@Test
	public void testVuota(){
		stringCalculator = new StringCalculator();
		assertEquals(0,stringCalculator.add(""));
		
		
	}
	
	
	@Test
	public void testUnNumero(){
		stringCalculator = new StringCalculator();
		assertEquals(1,stringCalculator.add("1"));
		assertEquals(5,stringCalculator.add("5"));
		
		
	}

	@Test
	public void testDueNumeri(){
		stringCalculator = new StringCalculator();
		assertEquals(5, stringCalculator.add("2,3"));
		assertEquals(7 ,stringCalculator.add("5,2"));
		
		
	}
	
	@Test
	public void testPiuNumeri(){
		stringCalculator = new StringCalculator();
		assertEquals(6, stringCalculator.add("1,2,3"));
		
		
	}
	
	
	@Test
	public void testBuffer(){
		stringCalculator = new StringCalculator();
		assertEquals(6, stringCalculator.add("1,2,3"));
		
		Integer[] arrayTest = { 1,2,3 };
		assertArrayEquals(arrayTest, stringCalculator.getLastSummands());
		
	}
	
	@Test
	public void testSeparatori(){
		stringCalculator = new StringCalculator();
		assertEquals(3, stringCalculator.add("//;\1;2"));
		
		
	}
	
}
