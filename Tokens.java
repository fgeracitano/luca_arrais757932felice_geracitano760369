import java.util.StringTokenizer;

public class Tokens {
	StringTokenizer token;
	int numero_token;
	int somma;
	
	public Tokens(String stringa){
		token = new StringTokenizer(stringa,",");
		numero_token = token.countTokens();
	};
	
	int numeroToken() {
		return numero_token;
		
	}

	public int somma() {
		

		while(token.hasMoreTokens())
		      this.somma = this.somma + this.nextIntToken();
		
		return this.somma;
	};
	
	public int somma_addendi(Integer[] array_addendi){
		
		int somma=0;
		for (int i=0; i<array_addendi.length; i++){
			somma=somma+array_addendi[i];
		}
		return somma;
	}
	
	
	public int nextIntToken(){
		String s= token.nextToken();
		
		return Integer.parseInt(s);
	}
	

}
